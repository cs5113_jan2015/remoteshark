#RemoteShark#
This application provides a way to monitor and capture packets on several remote machines. The user (client) can then view captured packets in the web browser and apply filters on these packets à la __Wireshark__.

##Pre-requisites##
1. The remote machines to be monitored need to have some distro of Linux installed. 
2. They should also have `python` installed (default on most distros).
3. Also install `tshark` on server.
4. `iwconfig` should also be installed.

##Running instructions##
##### Remote Machine side#####
Run server.py with `sudo python server.py` in your terminal.

#####Client side#####
1. __Start Program:__ Open `index.html` in any web browser.
2. __Add Machine:__ To add machines to monitor simply click on __add icon__ and enter the IP address and password for the machine. Then choose an interface from the list of interfaces of the server to monitor. You can add multiple servers too in a similar fashion.
3. __Inspect Packets:__ Next we see all the packets in a list like in wireshark. To inspect a packet click on it and it will appear in the top right window. The information is separated layer-wise from ethernet frame to top-level protocol.
4. __Compare two packets:__ To compare two packets simply right click on the second packet and it will appear in the bottom right window. Now you can simultaneously view two packets in the top and bottom window.
5. __Filtering Packets:__ In the bottom left window you can enter filters for packets based on port number, IP address of source and destination and protocol. For the full list of filters refer below to filter section.

## Architecture ##
The application follows client-server architecture where each remote machine acts as a server sending captured packets to the client. When a user enters a particular IP address of a remote machine we create a __socket__ connection between the clients browser and the server using WebSockets.

##### Remote Machine side #####
Here we are running a python program which actively waits for new socket connections. When it receives a new connection it creates a new thread to handle that particular host. Now to capture packets it creates a new python sub-process which launces tshark and captures the live output.

The next phase is to strip away useless text from `tshark` output. After this we create a JSON for each packet and transmit it to the host.

##### Client side #####
Here we parse received JSON and display the packets in a good format to the user. We maintain a list of all received packets. Packet filtering is done entirely on client side. Filtering is done for each element in this list. This is again an exerecise in text processing.

When we click on pause button the connection between the client and server is terminated and so is the python subprocess on server side. However the list of packets is maintained fo rfurther viewing and filtering until user clicks on play button where we then make a new connection with the server again and populate the list with new packets.

##Filters##
We can filter packets based on the following criteria:

1. __proto__: The protocol of the packet. You can compare with any layer protocol. possible values are `udp`, `tcp`, `http`, `ipv4`, `ipv6`, `icmp`, `icmp6` and so on.
2. __mac_src__: The source MAC address.
3. __mac_dest__:  The destination MAC address.
4. __ip_src__: The source IP address.
5. __ip_dest__: The destination IP address.
6. __port_src__: The source port number.
7. __port_dest__: The destination port number.

Consider the following examples:

1. `ip_src == '192.168.36.22' || ip_src == '172.16.35.2' `
Here we allow only packets whose source is either 192.168.36.22 or 172.16.35.2.

2. `proto == 'udp' && port_src == 3128`
This captures udp packets with 3128 as source port.

Similarly you can create filters as combinations of these filters. All parameters are to be compared with strings but port is compared with an integer.

##Contacts##
If you wish to extend this project and have any issues regarding the implementation of any module please contact  
1. Ajay Brahmakshatriya - cs12b1004@iith.ac.in  
2. Bhatu Pratik - cs12b1010@iith.ac.in  
3. Samrat Phatale - cs12b1035@iith.ac.in  

