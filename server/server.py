import socket
import threading
import hashlib
import base64
import struct
import json
import subprocess
import os
import sys


def layer_identifier(s,mode):
	if s=="Transmission Control Protocol":
		return "TCP"
	elif s=="Internet Protocol Version 6":
		return "IPv6"
	elif s=="Internet Protocol Version 4":
		return "IPv4"
	elif s=="User Datagram Protocol":
		return "UDP"
	elif s=="Internet Control Message Protocol v6":
		return "ICMP6"
	elif s=="Internet Control Message Protocol":
		return "ICMP"
	elif s=="Hypertext Transfer Protocol":
		return "HTTP"
	else:
		if mode==1:
			return s
		else:
			return "other"

def read_s(conn):
	rec=conn.recv(2)
	if rec=="":
		return ""
	bits=''.join(("0"*8+format(ord(x), 'b'))[-8:] for x in rec)
	length=int(bits[9:16],2)
	if length == 126:
		rec=chr(0)*6+conn.recv(2)
		length=struct.unpack(">q",rec)[0]
	elif length == 127:
		rec=chr(0)*4+conn.recv(4)
		length=struct.unpacl(">q",rec)[0]
	mask_flag=0
	if bits[8]=="1":
		mask_flag=1	
	mask=[]
	if mask_flag:
		rec=conn.recv(4)
		mask=[ord(x) for x in rec]
	data=""
	for i in range(length):
		byte=conn.recv(1)
		if mask_flag:
			byte=chr(ord(byte)^mask[i%4])
		data+=byte
	#print length
	return data	

def send_s(conn,s):
	send_payload=s
	send_bytes=""
	send_bytes=chr(0b10000001)
	send_payload_length=len(send_payload)
	if send_payload_length <= 125:
		send_bytes+=chr(send_payload_length)
	elif send_payload_length <= 0b1111111111111111:
		send_bytes+=chr(126)
		len_0=send_payload_length & 255
		len_1=(send_payload_length>>8) & 255
		send_bytes+=chr(len_1)
		send_bytes+=chr(len_0)
	else:
		send_bytes+=chr(127)
		len_0=send_payload_length & 255
		len_1=(send_payload_length >> 8) & 255
		len_2=(send_payload_length >> 16) & 255
		len_3=(send_payload_length >> 24) & 255
		send_bytes+=chr(len_3)+chr(len_2)+chr(len_1)+chr(len_0)
	send_bytes+=send_payload
	conn.send(send_bytes)

DEVNULL=open(os.devnull,'wb')

def main_proc_handler(conn,proc):
	packet={"layers":{}}

	packet["src"]=""
	packet["dest"]=""
	packet["ip_src"]="-1"
        packet["ip_dest"]="-1"
        packet["mac_src"]="-1"
        packet["mac_dest"]="-1"
        packet["port_src"]=-1
        packet["port_dest"]=-1
	layer_n=1
	for line in iter(proc.stdout.readline, ''):
		text=str(line.rstrip())
		if text=="":
			if layer_n==1:
				continue
			layer_n=1
			resp={}
			resp["code"]=202
			resp["packet"]=packet
			send_d=json.dumps(resp)
			try:
				send_s(conn,send_d)
			except:
				pass
			packet={"layers":{}}
			packet["ip_src"]="-1"
			packet["ip_dest"]="-1"
			packet["mac_src"]="-1"
			packet["mac_dest"]="-1"
			packet["port_src"]=-1
			packet["port_dest"]=-1
			continue

		if text[0]==" " or ord(text[0]) >= ord('0') and ord(text[0]) <= ord('9'):
			continue
		if text.split(' ')[0]=="Frame":
			fno=int(text.split(':')[0].split(' ')[1])
			packet["frame_no"]=fno
		elif text.split(' ')[0]=="Data":
			continue
		else:
			protocol = text.split(",")[0]
			pt=layer_identifier(protocol,1)
			if pt=="Ethernet II":
				packet["mac_src"]=text.split('Src:',1)[1].split(')',1)[0].split('(',1)[1].strip()
				packet["mac_dest"]=text.split('Dst:',1)[1].split(')',1)[0].split('(',1)[1].strip()
			elif pt=="IPv4" or pt=="IPv6":
				packet["ip_src"]=text.split('Src:',1)[1].split(')',1)[0].split('(',1)[1].strip()
				packet["ip_dest"]=text.split('Dst:',1)[1].split(')',1)[0].split('(',1)[1].strip()
			elif pt=="TCP" or pt=="UDP":
				packet["port_src"]=int(text.split('Src Port:',1)[1].split(')',1)[0].split('(',1)[1].strip())
				packet["port_dest"]=int(text.split('Dst Port:',1)[1].split(')',1)[0].split('(',1)[1].strip())
				
			if pt=="Ethernet II" or pt=="IPv4" or pt=="IPv6":
				packet["src"]=text.split('Src:',1)[1].split('(',1)[0].strip()
				packet["dest"]=text.split('Dst:',1)[1].split('(',1)[0].strip()
			packet["layers"]["layer_"+str(layer_n)]={"protocol":layer_identifier(protocol,1)}
			packet["layers"]["layer_"+str(layer_n)]["data"]=text
			packet["TLP"]=layer_identifier(protocol,1)
			packet["disp_style"]=layer_identifier(protocol,0)
			layer_n+=1


def message_handler(conn,data,context):
	try:
		msg=json.loads(data)
	except:
		return context
	if msg["code"]==100:
		code=-1
		if msg["pass"]=="password":
			context["authenticated"]=1
			code=200
		else:
			code=300
		resp={"code":code}
		send_d=json.dumps(resp)
		send_s(conn, send_d)
		return context

	if context["authenticated"]==0:
		resp={}
		resp["code"]=301
		send_d=json.dumps(resp)
		send_s(conn, send_d)
		return context
		
	if msg["code"]==101:
		proc=subprocess.Popen(['iwconfig'], stdout=subprocess.PIPE, stderr=DEVNULL)
		interfaces=["lo"]
		for line in iter(proc.stdout.readline, ''):
			text=str(line.rstrip())
			if text=="":
				continue
			if text[0]==" ":
				continue
			interfaces+=[text.split(" ")[0]]
		print interfaces
		resp={}
		resp["code"]=201
		resp["interfaces"]=interfaces
		send_d=json.dumps(resp)
		send_s(conn, send_d)
	elif msg["code"]==102:
		interface=msg["interface"]
		if context["main_proc"]!=0:
			context["main_proc"].kill()
		
		proc = subprocess.Popen(['tshark','-V', '-i',interface,'not','port','7070'], stdout=subprocess.PIPE, stderr=DEVNULL)
		context["main_proc"]=proc
		threading.Thread(target=main_proc_handler, args=(conn, proc)).start()

	return context
 
def handler(conn,addr):
	header_string=conn.recv(102400).strip().split("\r\n\r\n")[0]
	header_string=header_string.split("\r\n")
	print header_string[0]
	header_data=header_string[1:]
	headers={}
	headers["Sec-WebSocket-Protocol"]=""
	for i in header_data:
		field=i.split(":",1)[0].strip()
		data=i.split(":",1)[1].strip()
		headers[field]=data	
	if "Sec-WebSocket-Key" not in headers:
		conn.close()
		return -1
	key=headers["Sec-WebSocket-Key"]
	magic="258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
	final_resp=hashlib.sha1(key+magic).digest()
	resp_headers="HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: "+base64.b64encode(final_resp)+"\r\nSec-WebSocket-Protocol: "+headers["Sec-WebSocket-Protocol"]+"\r\n\r\n"
	conn.send(resp_headers)
	context={"main_proc":0,"authenticated":0}
	
	while 1:
		data=read_s(conn)
		if data=="" or data[0]!="{":
			print "RECEIVED EMPTY BREAKING"
			break
			
		print data
		context=message_handler(conn,data,context)
	print "CLOSING CONNECTION"
	if context["main_proc"]!=0:
		print "KILLING MAIN PROC"
		context["main_proc"].kill()
		context["main_proc"]=0
	sys.stdout.flush()
	conn.close()
	return 0




s=socket.socket(2,1)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind(("0.0.0.0",7070))

s.listen(100)
while 1:
	(conn,addr)=s.accept()
	threading.Thread(target=handler, args=(conn,addr)).start()	
